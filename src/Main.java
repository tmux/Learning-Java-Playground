import java.util.Calendar;

/* Adam's amazing Java playground. */

public class Main {

	// Variables.
	static String firstName = "Adam "; // Simple String.
	static String lastName = "Barlow";
	static String name = firstName + lastName;
	static double age = 16.1; // Double lets you put a decimal onto a number.
	static int adding = 15; // This is the original number.
	static int adding1 = ++adding; // This is adding one to the original number.
	static int fiveaddfive = 5 + 5; // Simple math.
	static int fivetimesfive = 5 * 5; // Simple math
	static int one = 10; // int one
	static int two = 15; // int two

	public static void main(String[] args) {
         new Main().init(args);
	}

	public void init(String[] args) {
		System.out.println("Initializing code");
		messages();
		forLoop();
		days();
		arr();
	}
	
	 public void messages() {
		System.out.println("Hello World,");
		System.out.println("");
		System.out.println("My name is: " + name);
		System.out.println("And I am: " + age);
		System.out.println("Useless random stuff:");
		System.out.println("");
		System.out.println("5 + 5 = " + fiveaddfive);
		System.out.println("5 * 5 = " + fivetimesfive);
		System.out.println("15 + 1 = " + adding1);
		if (one == two) {
			System.out.println(one + " is equal to " + two);
		} else if (one > two) {
			System.out.println(one + " is greater than " + two);
			return;
		} else if (one >= two) {
			System.out.println(one + " is equal to or greater than " + two);
			return;
		} else if (one < two) {
			System.out.println(one + " is less than " + two);
			return;
		} else if (one <= two) {
			System.out.println(one + " is equal to or less than " + two);
		}
	}

	public void forLoop() {
		for (int i = 0; i < 1; i++) {
			System.out.println("Amazing 1 line printed :3");
		}
	}

	// One line comment, can used for commenting out code that may be throwing
	// errors such as NullPointerExceptions.

	/*
	 * Multiple line comment, can be used for commenting out a big chunk of
	 * code, but doesn't have to be over multiple lines, it can also be used
	 * like this:
	 */

	/* This is a multiple line comment turned into a one line comment. */

	public void days() {
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_WEEK);

		switch (day) {
		case Calendar.MONDAY:
			System.out.println("Today is: Monday.");
			break;
		case Calendar.TUESDAY:
			System.out.println("Today is: Tuesday.");
			break;
		case Calendar.WEDNESDAY:
			System.out.println("Today is: Wednesday.");
			break;
		case Calendar.THURSDAY:
			System.out.println("Today is: Thursday.");
			break;
		case Calendar.FRIDAY:
			System.out.println("Today is: Friday.");
			break;
		case Calendar.SATURDAY:
			System.out.println("Today is: Saturday.");
			break;
		case Calendar.SUNDAY:
			System.out.println("Today is: Sunday.");
			break;
		}
	}

	public void arr() {
		String[] myName = { "A", "d", "a", "m" }; // I understand this.
		int[] myAge = { 1, 6 };

		System.out.println("");
		System.out.println("My name in an array is: " + myName[0] + myName[1] + myName[2] + myName[3]);
		System.out.println("And my age in an array is: " + myAge[0] + myAge[1]);
	}

}